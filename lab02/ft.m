function [ G ] = ft(Y)
    N = size(Y, 2);
    G = [];
    X = 0:N-1;
    Hann = (0.5*(1 - cos(2*pi*X/(N-1))));
    for k = 0:N-1
       G = [G, sum(Y .* exp(-2*pi*X*k*1i/N) .* Hann)];
    end
end

