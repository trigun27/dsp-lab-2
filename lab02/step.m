function [ Y ] = step(T, X )
    X = abs(X);
    X = T - X;
    X = X ./ abs(X);
    Y = (X + 1) ./ 2;
    k = find(isnan(Y));
    Y(k) = 0;
end

